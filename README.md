GPSS
=====

Ссылки
---------
* [Лекции Кащеева](https://gitlab.com/vadimadr/gpss-stuff/raw/master/%D0%9B%D0%B5%D0%BA%D1%86%D0%B8%D0%B8_1.pdf)
* [Документация по GPSS](http://www.minutemansoftware.com/reference/reference_manual.htm)
* [Интерпретатор GPSS World](http://www.minutemansoftware.com/downloads/GPSS%20World%20Student%20Setup.msi)
* [Статейка на хабре](https://habrahabr.ru/post/192044/)
* [Примеры моделей GitHub](https://github.com/bosonbeard/Funny-models-and-scripts)